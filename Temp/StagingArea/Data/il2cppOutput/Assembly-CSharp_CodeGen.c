﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String GetWalletAddress::WalletAddress()
extern void GetWalletAddress_WalletAddress_mCA4EF723C09FF07F4D8D6D58661AFC5BC08025E2 (void);
// 0x00000002 System.Void GetWalletAddress::OnClick()
extern void GetWalletAddress_OnClick_m8B2CCF86704FBC143D456AE035D63E2A5EBA027C (void);
// 0x00000003 System.Void GetWalletAddress::.ctor()
extern void GetWalletAddress__ctor_mB8E1FFAE3AAACFA48DCCE011496DF8AE744EE6D2 (void);
static Il2CppMethodPointer s_methodPointers[3] = 
{
	GetWalletAddress_WalletAddress_mCA4EF723C09FF07F4D8D6D58661AFC5BC08025E2,
	GetWalletAddress_OnClick_m8B2CCF86704FBC143D456AE035D63E2A5EBA027C,
	GetWalletAddress__ctor_mB8E1FFAE3AAACFA48DCCE011496DF8AE744EE6D2,
};
static const int32_t s_InvokerIndices[3] = 
{
	4,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	3,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
